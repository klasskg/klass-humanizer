from setuptools import setup

setup(
    name='klass-humanizer',
    version='0.1.0',
    description="Klass Humanizer",
    long_description='Humanizer adjusted and fixed from django.contrib.humanizer',
    classifiers=[
        "Intended Audience :: Web Developers",
        "Programming Language :: Python :: 2.7",
        "Framework :: Django",
    ],
    keywords="khumanizer",
    author="Daniyar Chambylov",
    packages=[
        'khumanizer',
        'khumanizer.utils',
    ],
    include_package_data=True,
    install_requires=[
        'django<=1.6',
    ],
)
