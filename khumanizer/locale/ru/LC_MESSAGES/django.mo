��          �      <      �     �     �     �     �     �     
       	     #   (  -   L  #   z  -   �      �  *   �          2     Q  �  U  !   ?  !   a  0   �  /   �  /   �          4     7  f   E  f   �  l     l   �  \   �  \   J     �     �     �                           
                        	                                                  %d day %d days %d hour %d hours %d minute %d minutes %d month %d months %d week %d weeks %d year %d years ,  0 minutes a minute ago %(count)s minutes ago a minute from now %(count)s minutes from now a second ago %(count)s seconds ago a second from now %(count)s seconds from now an hour ago %(count)s hours ago an hour from now %(count)s hours from now naturaltime%(delta)s ago naturaltime%(delta)s from now now Project-Id-Version: django-core
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-05-03 09:53+0600
PO-Revision-Date: 2013-11-16 09:36+0000
Last-Translator: Jannis Leidel <jannis@leidel.info>
Language-Team: Russian (http://www.transifex.com/projects/p/django/language/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 %d день %d дня %d дней %d час %d часа %d часа %d минута %d минуты %d минут %d месяц %d месяца %d месяцев %d неделю %d недели %d недель %d год %d года %d лет ,  0 минут %(count)s минуту назад %(count)s минуты назад %(count)s минут назад через %(count)s минуту через %(count)s минуты через %(count)s минут %(count)s секунду назад %(count)s секунды назад %(count)s секунд назад через %(count)s секунду через %(count)s секунды через %(count)s секунд %(count)s час назад %(count)s часа назад %(count)s часов назад через %(count)s час через %(count)s часа через %(count)s часов %(delta)s назад через %(delta)s сейчас 